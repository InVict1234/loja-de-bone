﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Cadastrar
{
    class CadastrarBusiness
    {
        public int Salvar(CadastrarDTO dto)
        {
            if (dto.nm_cliente == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            //CadastrarDTO cat = this.ConsultarPorNome(dto.nome_cadastro);
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            CadastrarDatabase db = new CadastrarDatabase();
            return db.Salvar(dto);
        }


        public List<CadastrarDTO> Consultar()
        {
            CadastrarDatabase db = new CadastrarDatabase();
            return db.Consultar();
        }

        public List<CadastrarDTO> Listar()
        {
            CadastrarDatabase db = new CadastrarDatabase();
            return db.Listar();
        }

    }
}
