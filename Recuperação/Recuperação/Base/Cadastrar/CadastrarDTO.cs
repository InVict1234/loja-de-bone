﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Cadastrar
{
    class CadastrarDTO
    {
        public int ID_Cliente { get; set; }
        public string nm_cliente { get; set; }
        public DateTime dt_nascimento { get; set; }
        public string ds_cpf { get; set; }
        public string ds_tel { get; set; }
        public string ds_email { get; set; }
        
    }
}
