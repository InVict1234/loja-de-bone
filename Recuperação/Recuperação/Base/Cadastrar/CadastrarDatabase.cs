﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Cadastrar
{
    class CadastrarDatabase
    {
        public int Salvar(CadastrarDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (ID_Cliente, nm_cliente, dt_nascimento, ds_cpf, ds_tel, ds_email) 
                                   VALUES (@ID_Cliente, @nm_cliente, @dt_nascimento, @ds_cpf, @ds_tel, @ds_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("nm_cliente", dto.nm_cliente));
            parms.Add(new MySqlParameter("dt_nascimento", dto.dt_nascimento));
            parms.Add(new MySqlParameter("ds_cpf", dto.ds_cpf));
            parms.Add(new MySqlParameter("ds_tel", dto.ds_tel));
            parms.Add(new MySqlParameter("ds_email", dto.ds_email));
           

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<CadastrarDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cadastrar";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadastrarDTO> lista = new List<CadastrarDTO>();
            while (reader.Read())
            {
                CadastrarDTO dto = new CadastrarDTO();
                dto.ID_Cliente = reader.GetInt32("ID_Cliente");
                dto.nm_cliente = reader.GetString("nm_cliente");
                dto.dt_nascimento = reader.GetDateTime("dt_nascimento");
                dto.ds_cpf = reader.GetString("ds_cpf");
                dto.ds_tel = reader.GetString("ds_tel");
                dto.ds_email = reader.GetString("ds_email");
                

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<CadastrarDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadastrarDTO> lista = new List<CadastrarDTO>();
            while (reader.Read())
            {
                CadastrarDTO dto = new CadastrarDTO();
                dto.ID_Cliente = reader.GetInt32("ID_Cliente");
                dto.nm_cliente = reader.GetString("nm_cliente");
                dto.dt_nascimento = reader.GetDateTime("dt_nascimento");
                dto.ds_cpf = reader.GetString("ds_cpf");
                dto.ds_tel = reader.GetString("ds_tel");
                dto.ds_email = reader.GetString("ds_email");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

       
    }
}
