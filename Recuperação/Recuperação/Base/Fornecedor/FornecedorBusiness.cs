﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Fornecedor
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO dto)
        {
            if (dto.nm_fornecedor == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            //CadastrarDTO cat = this.ConsultarPorNome(dto.nome_cadastro);
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            FornecedorDatabase db = new FornecedorDatabase();
            return db.Salvar(dto);
        }


        public List<FornecedorDTO> Consultar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Consultar();
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();
        }
    }
}
