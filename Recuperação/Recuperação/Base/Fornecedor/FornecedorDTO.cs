﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Fornecedor
{
    class FornecedorDTO
    {
        public int ID_Fornecedor { get; set; }
        public string nm_fornecedor { get; set; }
        public string ds_email { get; set; }
        public string ds_cpf { get; set; }
        public string ds_compra { get; set; }
        public string ds_tel { get; set; }
    }
}
