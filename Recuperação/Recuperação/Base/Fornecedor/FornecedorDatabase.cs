﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Fornecedor
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor (ID_Fornecedor, nm_fornecedor, ds_email, ds_cpf, ds_tel, dt_compra) 
                                   VALUES (@ID_Fornecedor, @nm_fornecedor, @ds_email, @ds_cpf, @ds_tel, @dt_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Fornecedor", dto.ID_Fornecedor));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.nm_fornecedor));
            parms.Add(new MySqlParameter("dt_compra", dto.ds_compra));
            parms.Add(new MySqlParameter("ds_cpf", dto.ds_cpf));
            parms.Add(new MySqlParameter("ds_tel", dto.ds_tel));
            parms.Add(new MySqlParameter("ds_email", dto.ds_email));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.ID_Fornecedor = reader.GetInt32("ID_Fornecedor");
                dto.nm_fornecedor = reader.GetString("nm_fornecedor");
                dto.ds_compra = reader.GetString("ds_compra");
                dto.ds_cpf = reader.GetString("ds_cpf");
                dto.ds_tel = reader.GetString("ds_tel");
                dto.ds_email = reader.GetString("ds_email");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<FornecedorDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.ID_Fornecedor = reader.GetInt32("ID_Fornecedor");
                dto.nm_fornecedor = reader.GetString("nm_fornecedor");
                dto.ds_compra = reader.GetString("ds_compra");
                dto.ds_cpf = reader.GetString("ds_cpf");
                dto.ds_tel = reader.GetString("ds_tel");
                dto.ds_email = reader.GetString("ds_email");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
