﻿using Recuperação.Base.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO dto)
        {
            if (dto.fk_cliente == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            //PedidoDTO cat = this.ConsultarPorNome(dto.Nome);
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            PedidoDatabase db = new PedidoDatabase();
            return db.Salvar(dto);
        }


        //public void Remover(int id)
        //{
        //    PedidoDatabase db = new PedidoDatabase();
        //    db.Remover(id);
        //}


        //public CategoriaDTO ConsultarPorNome(string nome)
        //{
        //    CategoriaDatabase db = new CategoriaDatabase();
        //    return db.ConsultarPorNome(nome);
        //}


        public List<PedidoDTO> Consultar()
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Consultar();
        }


        public List<PedidoDTO> Listar()
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.Listar();
        }


    }
}

