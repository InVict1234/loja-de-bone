﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Recuperação.Base.Pedido
{
    public class PedidoConsultarView
    {
        public int id_pedido { get; set; }
        public int quantidade { get; set; }
        public string cliente { get; set; }
        public string forma_pag { get; set; }
        public decimal Total { get; set; }
        public string pedido_item { get; set; }
    }
}
