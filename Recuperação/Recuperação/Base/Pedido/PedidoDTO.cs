﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Pedido
{
    class PedidoDTO
    {
        public int ID_Pedido { get; set; }
        public int ds_qtd { get; set; }
        public string fk_produto { get; set; }

        public string fk_cliente { get; set; }
        
        
    }
}
