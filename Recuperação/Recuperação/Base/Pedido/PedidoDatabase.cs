﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Pedido
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (ID_Pedido, ds_qtd, fk_produto, fk_cliente) 
                                   VALUES (@ID_Pedido, @ds_qtd, @fk_produto, @fk_cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Pedido", dto.ID_Pedido));
            parms.Add(new MySqlParameter("ds_qtd", dto.ds_qtd));
            parms.Add(new MySqlParameter("fk_produto", dto.fk_produto));
            parms.Add(new MySqlParameter("fk_cliente", dto.fk_cliente));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        //public void Remover(int id)
        //{
        //    string script = @"DELETE FROM tb_categoria WHERE id_categoria = @id_categoria";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("id_categoria", id));

        //    Database db = new Database();
        //    db.ExecuteInsertScript(script, parms);
        //}


        public List<PedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.ID_Pedido = reader.GetInt32("ID_Pedido");
                dto.ds_qtd = reader.GetInt32("ds_qtd");
                dto.fk_produto = reader.GetString("fk_produto");
                dto.fk_cliente = reader.GetString("fk_cliente");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<PedidoDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.ID_Pedido = reader.GetInt32("ID_Pedido");
                dto.ds_qtd = reader.GetInt32("ds_qtd");
                dto.fk_produto = reader.GetString("fk_produto");
                dto.fk_cliente = reader.GetString("fk_cliente");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        //public CategoriaDTO ConsultarPorNome(string nome)
        //{
        //    string script = @"SELECT * FROM tb_categoria WHERE nm_categoria = @nm_categoria";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("nm_categoria", nome));

        //    Database db = new Database();
        //    MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

        //    CategoriaDTO dto = null;
        //    while (reader.Read())
        //    {
        //        dto = new CategoriaDTO();
        //        dto.Id = reader.GetInt32("id_categoria");
        //        dto.Nome = reader.GetString("nm_categoria");
        //    }
        //    reader.Close();

        //    return dto;
        //}
    }
}

