﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.nm_produto == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            //ProdutoDTO cat = this.ConsultarPorNome(dto.nm_produto);
            //if (cat != null)
            //{
            //    throw new ArgumentException("Categoria já existe no sistema.");
            //}

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }


        //public void Remover(int id)
        //{
        //    CategoriaDatabase db = new CategoriaDatabase();
        //    db.Remover(id);
        //}


        //public CategoriaDTO ConsultarPorNome(string nome)
        //{
        //    CategoriaDatabase db = new CategoriaDatabase();
        //    return db.ConsultarPorNome(nome);
        //}


        public List<ProdutoDTO> Consultar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar();
        }


        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }
    }
}
