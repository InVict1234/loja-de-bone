﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Produto
{
    class ProdutoDTO
    {
        public int ID_Produto { get; set; }
        public string nm_produto { get; set; }
        public decimal vl_preco { get; set; }
    }
}
