﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recuperação.Base.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (ID_Produto, nm_produto, vl_preco) 
                                   VALUES (@ID_Produto, @nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Produto", dto.ID_Produto));
            parms.Add(new MySqlParameter("nm_produto", dto.nm_produto));
            parms.Add(new MySqlParameter("vl_preco", dto.vl_preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        //public void Remover(int id)
        //{
        //    string script = @"DELETE FROM tb_categoria WHERE id_categoria = @id_categoria";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("id_categoria", id));

        //    Database db = new Database();
        //    db.ExecuteInsertScript(script, parms);
        //}


        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID_Produto = reader.GetInt32("ID_Produto");
                dto.nm_produto = reader.GetString("nm_produto");
                dto.vl_preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<ProdutoDTO> Consultar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID_Produto = reader.GetInt32("ID_Produto");
                dto.nm_produto = reader.GetString("nm_produto");
                dto.vl_preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        //public CategoriaDTO ConsultarPorNome(string nome)
        //{
        //    string script = @"SELECT * FROM tb_categoria WHERE nm_categoria = @nm_categoria";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("nm_categoria", nome));

        //    Database db = new Database();
        //    MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

        //    CategoriaDTO dto = null;
        //    while (reader.Read())
        //    {
        //        dto = new CategoriaDTO();
        //        dto.Id = reader.GetInt32("id_categoria");
        //        dto.Nome = reader.GetString("nm_categoria");
        //    }
        //    reader.Close();

        //    return dto;
        //}
    }
}
