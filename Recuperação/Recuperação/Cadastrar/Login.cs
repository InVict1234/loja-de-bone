﻿using Recuperação.Cadastrar;
using Recuperação.Funcionarios;
using Recuperação.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recuperação.Telas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string senha = txtSenha.Text;
            string login = txtEmail.Text;


            if (login == "cmlcostumes@gmail.com" && senha == "1010")
            {
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Login ou senha Incorretos!");
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void sobreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar frm = new frmPedidoCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar frm = new frmPedidoConsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }
    }
}
