﻿using Recuperação.Base.Fornecedor;
using Recuperação.Cadastrar;
using Recuperação.Pedido;
using Recuperação.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recuperação.Funcionarios
{
    public partial class frmFuncionarioCadastrar : Form
    {
        public frmFuncionarioCadastrar()
        {
            InitializeComponent();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void sobreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar frm = new frmPedidoCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar frm = new frmPedidoConsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void frmFuncionarioCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.nm_fornecedor = txtNome.Text;
                dto.ds_cpf = txtCPF.Text;
                dto.ds_tel = txtTel.Text;
                dto.ds_email = txtEmail.Text;

                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(dto);

                MessageBox.Show("Cliente Salvo com sucesso.", "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);


                //this.ClienteCriado = cliente;
                //this.Close();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }
    }
}
