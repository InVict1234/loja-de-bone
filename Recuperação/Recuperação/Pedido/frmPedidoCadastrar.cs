﻿using Recuperação.Base.Cadastrar;
using Recuperação.Base.Pedido;
using Recuperação.Base.Produto;
using Recuperação.Cadastrar;
using Recuperação.Funcionarios;
using Recuperação.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recuperação.Pedido
{
    public partial class frmPedidoCadastrar : Form
    {
        public frmPedidoCadastrar()
        {
            InitializeComponent();
            carregarproduto();
            CarregarGrid();
            carregarcliente();
        }

        private void CarregarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar frm = new frmPedidoConsultar();
            frm.Show();
            Hide();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }
        private void carregarproduto()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> produtos = business.Consultar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID_Produto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.nm_produto);
            cboProduto.DataSource = produtos;
        }
        private void carregarcliente()
        {
            CadastrarBusiness business = new CadastrarBusiness();
            List<CadastrarDTO> clientes = business.Consultar();

            cboCliente.ValueMember = nameof(CadastrarDTO.ID_Cliente);
            cboCliente.DisplayMember = nameof(CadastrarDTO.nm_cliente);
            cboCliente.DataSource = clientes;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

                int qtd = Convert.ToInt32(txtQuantidade.Text);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                CadastrarDTO cliente = cboCliente.SelectedItem as CadastrarDTO;

                PedidoDTO dto = new PedidoDTO();
                ProdutoDTO produto = new ProdutoDTO();
                dto.fk_produto = Convert.ToString(cboProduto.SelectedValue);
                dto.fk_cliente = Convert.ToString(cboCliente.SelectedValue);
                dto.ds_qtd = Convert.ToInt32(txtQuantidade.Text);
              

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Pedido salvo com sucesso.", "CML", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void sobreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar frm = new frmPedidoCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            frmPedidoConsultar frm = new frmPedidoConsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }

        private void cboCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
