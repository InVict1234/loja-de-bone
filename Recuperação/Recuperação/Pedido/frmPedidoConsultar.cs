﻿using Recuperação.Base.Pedido;
using Recuperação.Cadastrar;
using Recuperação.Funcionarios;
using Recuperação.Telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recuperação.Pedido
{
    public partial class frmPedidoConsultar : Form
    {
        public frmPedidoConsultar()
        {
            InitializeComponent();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar frm = new frmPedidoCadastrar();
            frm.Show();
            Hide();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness business = new PedidoBusiness();
                List<PedidoDTO> lista = business.Consultar();

                dgvFornecedor.AutoGenerateColumns = false;
                dgvFornecedor.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sobreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void novoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmPedidoCadastrar frm = new frmPedidoCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar frm = new frmPedidoConsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }
    }
}
