﻿using Recuperação.Base.Produto;
using Recuperação.Cadastrar;
using Recuperação.Funcionarios;
using Recuperação.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recuperação.Telas
{
    public partial class ProdutosCadastrar : Form
    {
        public ProdutosCadastrar()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void ProdutosCadastrar_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
               

                ProdutoDTO dto = new ProdutoDTO();
                dto.nm_produto = txtProduto.Text.Trim();
                dto.vl_preco = Convert.ToDecimal(txtpreco.Text.Trim());
                

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso.", "DBZ",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde." + ex.Message, "CML",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();


        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void sobreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Sobre frm = new Sobre();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            ProdutosCadastrar frm = new ProdutosCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            Produtosconsultar frm = new Produtosconsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioCadastrar frm = new frmFuncionarioCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            frmFuncionarioConsultar frm = new frmFuncionarioConsultar();
            frm.Show();
            Hide();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar frm = new frmPedidoCadastrar();
            frm.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmPedidoConsultar frm = new frmPedidoConsultar();
            frm.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrar frm = new frmCadastrar();
            frm.Show();
            Hide();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void funcionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            frm.Show();
            Hide();
        }
    }
}
